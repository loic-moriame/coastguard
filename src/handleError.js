import { red, green } from 'chalk';
import { EOL } from 'os';

export default async (message, error) => {
  let errorMessage = 'Unknown error occurred';

  if (error instanceof Response) {
    const { ErrorMessage } = await error.json();

    if (ErrorMessage) {
      errorMessage = ErrorMessage;
    }
  } else {
    errorMessage = error.message;
  }

  message = message || errorMessage;

  process.stderr.write(red(`Error: ${message}`) + EOL);
  process.stderr.write(
    `Hint: Use the ${green(
      '--help',
    )} option to get help about the usage` + EOL,
  );
  process.exit(1);

}