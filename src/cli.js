#!/usr/bin/env node

import yargs from "yargs";
import { hideBin } from "yargs/helpers";
import { commands } from "./command/commands.js";

const argv = yargs(hideBin(process.argv))
  .scriptName("costguard")
  .usage("$0 <command> [args]")
  .command(commands)
  .demandCommand(1, "")
  .example(
    "$0 sales doodles-official",
    "retrieve the last sale for collection <doodles>"
  )
  .alias({ h: "help" })
  .alias({ v: "version" })
  .epilogue("Made with ❤ by 0xWisrou")
  .parse();
