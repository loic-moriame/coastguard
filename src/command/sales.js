import fetch from "node-fetch";
import chalk from "chalk";
import db from "../db.js";
import ora from "ora";

let database;
let spinner;
let interval;
//  yargs required export:
//      const command = <string>
//      const desc = <string>
//      builder(yargs) = fn
//      handler(argv) = fn
export const command = "sales <collection>";
export const desc = `Monitor new sales for <collection>`;
export function builder(yargs) {}
export async function handler(argv) {
  const collection = argv.collection;
  const frequency = argv.frequency || 1000;
  database = new db(collection);

  spinner = ora("monitoring new sales...").start();
  interval = setInterval(() => {
    monitorNewSale(collection);
  }, frequency);
}

async function monitorNewSale(collection) {
  try {
    const previous_sale = await database.get("last_sale");
    const last_sale = await getLastSale(collection);
    console.log(previous_sale);
    console.log(last_sale);

    if (JSON.stringify(previous_sale) !== JSON.stringify(last_sale)) {
      spinner.succeed("new sale detected!");
      console.log(last_sale);
      spinner.start();
    }
    return Promise.resolve();
  } catch (error) {
    spinner.fail();
    clearInterval(interval);
    console.log(chalk.redBright(error));
    // return new Error(error);
  }
}

async function getLastSale(collection) {
  const url = `https://api.opensea.io/api/v1/assets?collection=${collection}&offset=0&limit=1&order_by=sale_date&order_direction=desc`;
  const response = await fetch(url);
  const data = await response.json();

  if (data.assets.length <= 0) {
    return Promise.reject(`unknown collection ${collection}`);
    return console.log(chalk.red(`unknown collection ${collection}`));
  }

  //  instantiate a new database for <collection>
  let last_sale = {};
  data.assets.forEach((asset) => {
    const price =
      asset.last_sale.total_price /
      Math.pow(10, asset.last_sale.payment_token.decimals);

    last_sale = {
      token: asset.name || `#${asset.token_id}`,
      price: `${+price.toFixed(3)} ${asset.last_sale.payment_token.symbol}`,
      image_url: asset.image_url,
      date: asset.last_sale.transaction.timestamp,
    };
  });
  await database.set("last_sale", last_sale);
  return Promise.resolve(last_sale);
}

async function tweet() {}
