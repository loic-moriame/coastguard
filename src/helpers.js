import fs from "fs";
import envPaths from "env-paths";
import makeDir from "make-dir";

export const config = envPaths("coastguard");

//  create default folders if missing
(async () => {
  if (
    !fs.existsSync(config.cache) ||
    !fs.existsSync(config.config) ||
    !fs.existsSync(config.data) ||
    !fs.existsSync(config.log) ||
    !fs.existsSync(config.temp)
  ) {
    await Promise.all([
      makeDir(config.cache),
      makeDir(config.config),
      makeDir(config.data),
      makeDir(config.log),
      makeDir(config.temp),
    ]);
  }
})();
