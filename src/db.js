import fs from "fs";
import { join } from "path";
import { Low, JSONFile, LowSync, JSONFileSync } from "lowdb";
import chalk from "chalk";
import { config } from "./helpers.js";

export default class database {
  constructor(name) {
    this.path = join(config.data, `${name}.json`);
    this.db = new Low(new JSONFile(this.path));

    //  init database if not existing yet
    if (!fs.existsSync(this.path)) {
      console.log(chalk.yellow("database initialize..."));
      const db = new LowSync(new JSONFileSync(this.path));
      db.read();
      db.data ||= {};
      db.write();
    } else {
      // console.log(chalk.yellow(`database alrady exists as ${this.path}`));
    }
  }

  async set(key, value) {
    await this.db.read();
    this.db.data[key] = value;
    await this.db.write();
  }

  async get(key) {
    await this.db.read();
    return (this.db.data[key] ||= null);
  }

  async has(key) {}
}
